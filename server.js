// server.js
const crypto = require('crypto')
const fastify = require('fastify')({ logger: true })

const SSO_KEY = '255b1d50658f42ecbcb79bd3872fe0cc'

/**
 * Validate signature using HMAC+SHA256+HEX
 * @param {string} ssoKEY 
 * @param {string} payload 
 * @param {string} signature 
 * @returns {boolean}
 */
function validateSignatureHMAC(ssoKEY, payload, signature) {
  var hmacsha56 = crypto
    .createHmac('sha256', ssoKEY)
    .update(payload)
    .digest('hex')

  return hmacsha56 === signature
}

// Define the /webhook route
fastify.post('/webhook', async (request, reply) => {
  const payload = request.body
  const headers = request.headers

  if (validateSignatureHMAC(SSO_KEY, JSON.stringify(payload), request.headers['x-signature'])) {
    console.log('Received webhook headers:', headers)
    console.log('Received webhook payload:', payload)

    // Send a response back to the sender
    return await reply.code(200).send({
      success: true,
      message: 'Webhook successfully received',
      validation: true
    })
  }

  // Send a response back to the sender
  return await reply.code(400).send({
    success: false,
    message: 'Signature not match!',
    validation: false
  })
})

// Start the server
const start = async () => {
  try {
    await fastify.listen({ host: '0.0.0.0', port: 5555 });
    fastify.log.info(`Server listening on http://localhost:5555`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();