# webhook-fastify
An example how to listen the webhook from OpenSSO using Fastify framework.


### Usage
To use this example script, make sure you have successfully running your OpenSSO v.1.5.0.

### How to activate Webhook

1. Login to your OpenSSO
2. Go to menu `My SSO`.
3. Add your new SSO.  
![](https://gitlab.com/nanowebdev/example/opensso/webhook-fastify/-/raw/main/img/tutorial-1.png)
4. Click Submit, Now you have a `SSO Key`.  
![](https://gitlab.com/nanowebdev/example/opensso/webhook-fastify/-/raw/main/img/tutorial-2.png)
5. Done.

To activate a webhook, just simply put your webhook url endpoint.

### How to setup your webhook listener
1. Edit file `server.js` see line 5, replace **SSO_KEY** with yours.
2. Build this source by running `npm install`
3. To start this server, just run `npm start`.


### Need Help
Just chat with me via Telegram >> [https://t.me/aalfiann](https://t.me/aalfiann)

